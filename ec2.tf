resource "aws_instance" "terraform-test" {
  ami           = "ami-04b70fa74e45c3917"
  instance_type = "t2.nano"
  key_name = "temp-teste"
  security_groups = ["allow_ssh", "allow_http", "allow_egress"]
  user_data = file("script.sh")

  tags = {
    Name = "GitLabRunner01"
    "Business Unit" = "CI/CD"
    Environment = "test"
    Customer = "Nuxstep"
  }
}