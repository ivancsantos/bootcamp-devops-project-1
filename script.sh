#!/bin/bash

echo "Atualizando pacotes do sistema operacional..."
apt update -y &> /dev/null

echo "Instalando novos pacotes necessários..."
apt install -y git vim &> /dev/null

echo "Habilitando serviço HTTPD..."
sudo systemctl enable httpd &> /dev/null

echo "Iniciando serviço HTTPD..."
sudo systemctl start httpd &> /dev/null

echo "Instalando Docker..."
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

echo "Instalando o GitLab Runner..."
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install -y gitlab-runner
sudo usermod -aG docker gitlab-runner