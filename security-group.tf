resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  #vpc_id      = aws_vpc.main.id # variavel

  tags = {
    Name = "allow_ssh"
  }
  
  ingress {
	  description = "SSH from any"
	  from_port = 22
	  to_port = 22
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	  #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block] # variavel
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  #vpc_id      = aws_vpc.main.id # variavel

  tags = {
    Name = "allow_http"
  }
  
  ingress {
	  description = "HTTP from any"
	  from_port = 80
	  to_port = 80
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	  #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block] # variavel
  }
}

resource "aws_security_group" "allow_egress" {
  name        = "allow_egress"
  description = "Allow egress traffic"
  #vpc_id      = aws_vpc.main.id # variavel

  tags = {
    Name = "allow_egress"
  }
  
  egress {
	  from_port = 0
	  to_port = 0
	  protocol = "-1"
	  cidr_blocks = ["0.0.0.0/0"]
	  ipv6_cidr_blocks = ["::/0"]
  }
}